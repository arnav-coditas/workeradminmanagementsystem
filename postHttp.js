class Http1 {
    #baseUrl = "https://4656-2402-8100-30a2-adef-d01a-c6fe-fa74-c023.in.ngrok.io";
    
    async send(endpoint, options = {},token=null) {
        try {
            options = { 
                ...options,
                headers: {
                    "Authorization":  `Bearer ${token}`,
                    "ngrok-skip-browser-warning":"1234",
                    'Content-Type': 'application/json'
                },

            
                // body: data ? JSON.stringify(data) : null  
            }
            
            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
                  const parsedData = await response.json();
          
            return parsedData;
        } catch (e) {
            throw e;
        }
    }

    async get(endpoint,token) {
        return await this.send(endpoint,{ method: 'get' },token);
        
    }

    async post(endpoint,data,token) {

        console.log(data)
       
        const ans=await this.send(endpoint, { method: 'POST' },data,token);

      return ans;
    } 


    async delete(endpoint,data,token) {
        return await this.send(endpoint, { method: 'DELETE' },data,token);
    }




}

const httpone = new Http1();

export default httpone;



