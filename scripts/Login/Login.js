import http from "../../http.js";
import { $ } from "../../dom.js";
import AdminSection from "../Dashboard/workerRow.js";
import studentSection from "../Dashboard/workerRow.js"
import trainerSection from "../Dashboard/workerRow.js"
import httpone from "../../postHttp.js";
import b from "../Dashboard/Admin.js";
import viewAdmin from "../Dashboard/Admin.js";
import viewstudent from "../Dashboard/student.js";
import  viewTrainer from "../Dashboard/Trainer.js"

export function createLoginForm() {
  const loginauthenticate = async (event) => {
    try {
      event.preventDefault();
      const data = {
        email: $("#username").value,
        password: $("#password").value,
      };
      const authenticatedResponse = await http["post"]("authenticate", data);
      const a = authenticatedResponse;

      console.log(a); 

      if (a.userRole === "ADMIN") {
        AdminSection["AdminSection"]();
        $("#worker").addEventListener("click", () => {
          viewAdmin(a.jwtToken);
         
        });

        GiveToken(a.jwtToken);
      }
      else if(a.userRole==="STUDENT"){
        studentSection["studentSection"]();
        $("#worker").addEventListener("click", () => {
          viewstudent(a.jwtToken);
        });
      }


else if(a.userRole==="TRAINER"){
  trainerSection["trainerSection"]();
  $("#worker").addEventListener("click", () => {
    viewTrainer(a.jwtToken);
   
  });
}

    } catch(e) {
      const DisplayErrorOne = `
      <section class="login">
      <h1>INVALID CREDETIALS! <h1>
      <button  id="tryAgain">Try Again</button>
`;
      $(".Modules").innerHTML = DisplayErrorOne;
    }
  };
  const DisplayError = `<section class="main">
    <section class="login">
    <h2>Login</h2>
    <form action="">
        <label for="username">Username</label>
        <input type="text"  id="username">
        <label for="password">Password</label>
        <input type="password" id="password">
        <button type="submit" id="submit">Submit</button>

    </form>
    </section>
</section>`;
  $(".Modules")[0].innerHTML = DisplayError;
  $("#submit").addEventListener("click", loginauthenticate);
}



