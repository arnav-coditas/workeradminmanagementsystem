import httpone from "../../postHttp.js";
import { $ } from "../../dom.js";
import http from "../../http.js";

const viewstudent = async (token) => {
  try {
    //  console.log(await httpone.get("trainer/getFeedback/1", token));
    //  const ax = await httpone.get("trainer/getFeedback/1", token);

    let WorkerTableData = ``;

    let tableheadings = `  <tr>

<th> Trainer Name</th>
<th> Trainer Id</th>
<th> Trainer Email</th>
<th>Trainer Salary</th>
<th><button id="addWorker">Add Trainer</button></th>

</tr>`;
    for (let key in ax) {
      WorkerTableData += `
     
  
<td>${ax[key]["trainerId"]}</td>
<td>${ax[key]["trainerName"]}</td>
<td>${ax[key]["trainerEmail"]}</td>
<td>${ax[key]["trainerSalary"]}</td>
<td><button id="id${ax[key]["workerId"]}">Delete</button></td>
<td><button id="edit${ax[key]["workerId"]}">Edit</button></td>
<td><button id="view${ax[key]["trainerId"]}">View Students</button></td>
</tr>`;
    }

    $("#WorkerData").innerHTML = tableheadings + WorkerTableData;

    for (let key in ax) {
      $(`#id${ax[key]["workerId"]}`).addEventListener("click", async (e) => {
        console.log(`#id${ax[key]["workerId"]}`);
        const id = `#id${ax[key]["workerId"]}`;
        console.log(id);
        let idNum = id.match(/\d/g).join("");
        console.log(idNum);
        httpone.delete(`admin/deleteWorker/${idNum}`, token);
        console.log("Deleted");
        e.preventDefault();
      });

      $(`#view${ax[key]["trainerId"]}`).addEventListener("click", async (e) => {
        for (let key1 in ax) {
          console.log(ax[key]["students"][key1].studentName);
          let StudentTableData = ``;
          StudentTableData += `
   <tr>
 <th>Student Name</th>
 <th>Student Email</th>
 <th>Domain</th>
 <th><button id="back">Back</button></th>
     </tr>
       <tr>
       <td>${ax[key]["students"][key1].studentName}</td>
        <td>${ax[key]["students"][key1].studentEmail}</td>
        <td>${ax[key]["domain"].domainName}</td>
        </tr>`;
          $("#WorkerData").innerHTML = StudentTableData;

          $("#back").addEventListener("click", () => {
            viewstudent(token);
          });
          // console.log(`clicked on #view${ax[key]["trainerId"]}`)
        }
      });
    }

    $("#order").addEventListener("click", async () => {
      const bx = await httpone.get("admin/admin/getStudents", token);
      let OrderTableData = "";
      let table = "";

      for (let key in bx) {
        console.log(bx);
        OrderTableData += `

        <tr>
      <th>OrderId</th>
      <th>Order  Status</th>
      <th>Total  Price</th>
      <th>Assigned to WorkerId:</th>
          </tr>
            <tr>
            <td>${bx[key].worker_orders[key].orderId}</td>
             <td>${bx[key].worker_orders[key].orderStatus}</td>
             <td>${bx[key].worker_orders[key].orderTotalPrice}</td>
             <td>${bx[key].workerId}</td>
             </tr>`;

        $("#WorkerData").innerHTML = OrderTableData;
      }
    });

    $("#addWorker").addEventListener("click", () => {
      let form = "";
      form = `
    <section class="addnew-form active" id="form-update">
    <form action="" class="">

        <label for="Name">Name</label>
        <input type="text" name="" id="name">
        <label for="username">email</label>
        <input type="text" name="" id="email">
        <label for="number">Salary</label>
        <input type="number" name="" id="salary">
        <label for="branch">Password</label>
        <input type="text" name="" id="password">
        <label for="number">Salary</label>
        <input type="number" name="" id="salary">
        
      
        <button id="submitnow" type="submit">Submit</button>
        <button  id="close">Close</button>
    </form>
</section>
`;

      $("#WorkerData").innerHTML = form;
      $("#close").addEventListener("click", () => {
        ($("#WorkerData").innerHTML = ""), viewstudent(token);
      });

      $("#submitnow").addEventListener("click", async (e) => {
        e.preventDefault();
        const name = $("#name").value;
        const email = $("#email").value;
        const password = $("#password").value;
        const salary = $("#salary").value;
        console.log(name, email, password, salary);
        let data = {
          trainerName: `${name}`,
          trainerEmail: `${email}`,
          trainerSalary: `${password}`,
          trainerPassword: `${salary}`,
          employmentTypeId: 1,
          domainId: 4,
        };

        console.log(token);
        await http.post("admin/addTrainer", data);
      });
    });

    for (let key in ax) {
      $(`#edit${ax[key]["workerId"]}`).addEventListener("click", () => {
        let form = "";
        form = `
<section class="addnew-form active" id="form-update">
<form action="" class="">

    <label for="Name">Name</label>
    <input type="text" name="" id="name" value=${ax[key]["workerName"]}>
    <label for="username">Userame</label>
    <input type="text" name="" id="username" value="${ax[key]["workerUsername"]}">
    <label for="branch">Password</label>
    <input type="text" name="" id="password" value="${ax[key]["workerPassword"]}">
    <label for="percentage">Salary</label>
    <input type="number" name="" id="salary" value="${ax[key]["workerSalary"]}">
    <button id="submitnow" type="submit">Submit</button>
    <button  id="close">Close</button>
</form>
</section>
`;

        $("#WorkerData").innerHTML = form;
        $("#close").addEventListener("click", () => {
          ($("#WorkerData").innerHTML = ""), viewstudent(token);
        });

        $("#submitnow").addEventListener("click", async (e) => {
          e.preventDefault();
          const workerName = $("#name").value;
          const workerUsername = $("#username").value;
          const workerPassword = $("#password").value;
          const workerSalary = $("#salary").value;
          console.log(workerName, workerUsername, workerSalary);
          let data = {
            workerName: `${workerName}`,
            workerUsername: `${workerUsername}`,
            workerPassword: `${workerPassword}`,
            workerSalary: `${workerSalary}`,
          };

          console.log(token);
          await http.put("admin/updateWorker", data);
        });
      });
    }

    $("#tools").addEventListener("click", async () => {
      const cx = await httpone.get("admin/getTools", token);

      let table = "";
      table = `<tr>
<th>Tool id</th>
<th>Tool Name</th>
<th>Total Size</th>
<th>Tool Price</th>

</tr>`;
      let ToolsTableData = "";

      for (let key in cx) {
        ToolsTableData += `
            <tr>
            <td>${cx[key].toolId}</td>
             <td>${cx[key].toolName}</td>
             <td>${cx[key].toolSize}</td>
             <td>${cx[key].toolPrice}</td>
             <td><button id="del${cx[key].toolId}">Delete</button></td>
             <td><button id="editbtn${cx[key].toolId}">Edit</button></td>
        </tr>`;
        $("#WorkerData").innerHTML = table + ToolsTableData;
      }

      for (let key in cx) {
        $(`#editbtn${cx[key].toolId}`).addEventListener("click", () => {
          let form = "";
          form = `
    <section class="addnew-form active" id="form-update">
    <form action="" class="">
    
        <label for="Name">Tool id</label>
        <input type="text" name="" id="toolid" value="${cx[key].toolId}">
        <label for="username">Tool Name</label>
        <input type="text" name="" id="toolname" value="${cx[key].toolName}">
        <label for="branch">Tool size</label>
        <input type="text" name="" id="toolsize" value="${cx[key].toolSize}">
        <label for="percentage">Tool price</label>
        <input type="number" name="" id="price" value="${cx[key].toolPrice}">
        <button id="submitnow" type="submit">Submit</button>
        <button  id="close">Close</button>
    </form>
    </section>

    `;

          $("#WorkerData").innerHTML = form;

          $("#close").addEventListener("click", () => {
            ($("#WorkerData").innerHTML = ""), viewstudent(token);
          });
        });
      }

      let idNum = id.match(/\d/g).join("");
      console.log(idNum);
      httpone.delete(`admin/deleteTool/${idNum}`, token);
    });
  } catch {}
};

export default viewstudent;
