 import {$} from "../../dom.js"
import { createLoginForm } from "../Login/Login.js"
import http from "../../http.js"
import httpone from "../../postHttp.js"

 const  AdminSection = () => {
   
    const DashboardTemplate =
        `<section class="parent">
        <section class="navigation">
            <div class="subnavigation">
                <h2> Admin Dashboard</h2>
                <span id="worker">View Trainers</span>
                <span id="orders">View Students</span>
                <span id="logout">LogOut</span>
            </div>
        </section>
        <div id="table-container1">
        <table>
            <tbody class="worker-details-body">
            </div>
            <tbody id="WorkerData">
            </tbody>
            </tbody>
            </table>
    </section>
        `
        console.log($(".Modules")[0]);
        $(".Modules")[0].innerHTML=DashboardTemplate;
        $("#logout").addEventListener("click",createLoginForm)

}   


const  studentSection = () => {
   
    const DashboardTemplate =
        `<section class="parent">
        <section class="navigation">
            <div class="subnavigation">
                <h2> Student Dashboard</h2>
                <span id="worker">Give Feedback</span>
             
                <span id="logout">LogOut</span>
            </div>
        </section>
        <div id="table-container1">
        <table>
            <tbody class="worker-details-body">
            </div>
            <tbody id="WorkerData">
            </tbody>
            </tbody>
            </table>
    </section>
        `
        console.log($(".Modules")[0]);
        $(".Modules")[0].innerHTML=DashboardTemplate;
        $("#logout").addEventListener("click",createLoginForm)

}   





const trainerSection = () => {
   
    const DashboardTemplate =
        `<section class="parent">
        <section class="navigation">
            <div class="subnavigation">
                <h2> Trainer Dashboard</h2>
                <span id="worker">View Feedback</span>
             
                <span id="logout">LogOut</span>
            </div>
        </section>
        <div id="table-container1">
        <table>
            <tbody class="worker-details-body">
            </div>
            <tbody id="WorkerData">
            </tbody>
            </tbody>
            </table>
    </section>
        `
        console.log($(".Modules")[0]);
        $(".Modules")[0].innerHTML=DashboardTemplate;
        $("#logout").addEventListener("click",createLoginForm)

}   



export default{
    AdminSection,
    studentSection,
    trainerSection
}