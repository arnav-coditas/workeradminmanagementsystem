class Http {
    #baseUrl = "https://4656-2402-8100-30a2-adef-d01a-c6fe-fa74-c023.in.ngrok.io";
    
    async send(endpoint, options = {}, data,token=null) {
        try {
            options = { 
                ...options,
                headers: {
                    // "Authorization": `Bearer ${token}`,
                    "ngrok-skip-browser-warning":"1234",
                    'Content-Type': 'application/json'
                },

            
                body: data= JSON.stringify(data)   
            }
            
            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
            const parsedData = await response.json();
            console.log(parsedData)
            return parsedData;
          
        } catch (e) {
            throw e;
        }
    }

    async get(endpoint,token) {
        return await this.send(endpoint,{ method: 'get' },token);
        
    }

    async delete(endpoint,token) {
        return await this.send(endpoint, { method: 'DELETE' },token);
    }

    async post(endpoint,data,token) {
        try{
  
        const ans=await this.send(endpoint, { method: 'POST', headers:{
               "Authorization": `Bearer ${token}`,
        }},data,token);
        return ans;
        }catch{
            
        }
    } 
    
    async put(endpoint,data, token) {
        return await this.send(endpoint, { method: 'PUT' },data,token)
        
    } 


}

const http = new Http();

export default http;



